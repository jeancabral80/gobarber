const { User } = require('../models')

class UserController {
  create (req, res) {
    return res.render('auth/signup')
  }

  async store (req, res) {
    if (req.file) {
      const { filename } = req.file
      await User.create({ ...req.body, avatar: filename })
    } else {
      await User.create({ ...req.body })
    }

    return res.redirect('/')
  }
}

module.exports = new UserController()
