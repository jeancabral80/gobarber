module.exports = {
  username: 'docker',
  password: 'docker',
  database: 'gobarber',
  host: 'db',
  dialect: 'postgres',
  operatorAliases: false,
  define: {
    timestamos: true,
    underscored: true,
    underscoredAll: true
  }
}
